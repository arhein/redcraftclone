#include <gtest/gtest.h>
#include "redcraft.h"

Redcraft* rc_default;
TEST(DefaultConfiguration, InitialState) {
    std::string test_path = std::string(TEST_PATH) + "sample_1a1z_1Hz/";
    rc_default = new Redcraft(test_path + "default.conf");
    struct RunState _1a1z = {
            .mCurrentDepth = 100,
            .mCurrentResidue = 1,
            .mResidueName = "ASP ",
            .mDecimationEnabled = false,
            .mLJEnabled = true,
            .mOTEstimation = false
    };
    EXPECT_EQ(rc_default->get_run_state(), _1a1z);
}

TEST(DefaultConfiguration, GenerateAngles) {
    // TODO: Decouple angle generation from stage1
    rc_default->run();
    rc_default->print_state();
}