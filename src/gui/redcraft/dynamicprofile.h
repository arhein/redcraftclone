#ifndef DYNAMICPROFILE_H
#define DYNAMICPROFILE_H

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QLegend>
#include <QtCharts/QValueAxis>

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <sstream>

class DynamicProfile : public QWidget
{
public:
    DynamicProfile(QWidget *parent = nullptr);
    void loadOut(QString path, int x, int y);
    void resizeEvent(QResizeEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
private:
    QtCharts::QChart *chart;
    QtCharts::QChartView *chartView;
    QString path;
    int x, y;
};

#endif // DYNAMICPROFILE_H
